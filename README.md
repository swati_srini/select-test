#Enhanced select fields

The objective is to transform the given simple select field into the field as given in the image "transformed.png" by writing a plugin that can be called with options.



1. Make a single select field, which gives the user an option to enter the text and shows relevant results based on text entered.
2. Make a multiple select field which allow user to select more than one value, in this case user will not be able to enter text, just select.

The 2 select fields are given in the html page and the plugin is applied on them in "test.js". Write your code in script.js and css code in styles.js.


OPtions accepted by plugin 

	"multi" : bool - signifies wether select is multi or single. False by default.
	"maxSelect": number - The number of selectable options in case of multi select. Default value = 1