// SImple select
$('select.singleCountrySelect').enhancedSelect();

//Multi select
$('select.multiCountrySelect').enhancedSelect({
		"multi": true,
		"maxSelect": 5
});